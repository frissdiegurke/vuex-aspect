const COMMIT_KEY_INFIX = " #plugins/vuex-aspect/";

export function getCommitKeys(id) {
  const prefix = id + COMMIT_KEY_INFIX;
  return {
    init: prefix + "init",
    begin: prefix + "begin",
    clear: prefix + "clear",
    error: prefix + "error",
    success: prefix + "success",
  };
}

export function getStateKeys(prefix) {
  return {
    success: prefix,
    error: prefix + "$error",
    loading: prefix + "$loading",
  };
}
