import commit from "../../commit";

export function init(aspect) {
  commit(
    aspect.$store,
    {type: aspect._commitKeys.init},
    () => {
      const state = aspect.$module.state, vm = aspect.$store._vm;
      if (!state.hasOwnProperty(aspect._stateKeys.success)) { vm.$set(state, aspect._stateKeys.success, void 0); }
      if (!state.hasOwnProperty(aspect._stateKeys.error)) { vm.$set(state, aspect._stateKeys.error, void 0); }
      if (!state.hasOwnProperty(aspect._stateKeys.loading)) { vm.$set(state, aspect._stateKeys.loading, false); }
    }
  );
}

export function loading(aspect, queryData) {
  commit(
    aspect.$store,
    {type: aspect._commitKeys.begin, payload: queryData},
    () => {
      const state = aspect.$module.state;
      state[aspect._stateKeys.loading] = true;
    }
  );
  return queryData;
}

export function response(aspect, queryData) {
  const keys = aspect._commitKeys;
  const type = queryData.hasFailed ? keys.error : queryData.isClear ? keys.clear : keys.success;
  commit(
    aspect.$store,
    {type, payload: queryData},
    () => {
      const state = aspect.$module.state;
      state[aspect._stateKeys.success] = queryData.value;
      state[aspect._stateKeys.error] = queryData.reason;
      if (queryData.isLatest) { state[aspect._stateKeys.loading] = false; }
    }
  );
}
