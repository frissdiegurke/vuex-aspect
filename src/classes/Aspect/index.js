import BoundWatcher from "../BoundWatcher";
import QueryData from "../QueryData";

import DEFAULT_OPTIONS from "./default-options";
import {getCommitKeys, getStateKeys} from "./keys";
import {init as commitInit, loading as commitLoading, response as commitResponse} from "./commits";
import mixins from "./mixins";

/**
 * The main class of vuex-aspect. Each instance is responsible for one query definition and its respective variables
 * watcher.
 */
export default class Aspect {

  /**
   * @public
   * Creates a new aspect to bind its resource to the vuex store whenever at least one consumer exists.
   *
   * @param {Object|Function} options The options. If a function is provided, it will be used as `resolve` option. Valid
   * options are:
   * <ul>
   *   <li>`{String} key`</li>
   *   <li>`{Object} watch`</li>
   *   <li>`{Function<*>} variables({{state,getters,rootState,rootGetters}} ctx)`</li>
   *   <li>`{AsyncFunction<*>} resolve({*} variables, {Object} context)`</li>
   *   <li>`{Function} enable()`</li>
   *   <li>`{Function} supply({variables, oldVariables, isInitial})`</li>
   *   <li>`{Function} disable()`</li>
   *   <li>`{Function} error({Object} queryData)`</li>
   * </ul>
   *
   * @constructor
   */
  constructor(options) {
    // apply options
    if (typeof options === "function") { options = {resolve: options}; }
    this._optionResolve = options.resolve || void 0;
    this._optionEnable = options.enable || DEFAULT_OPTIONS.enable;
    this._optionSupply = options.supply || DEFAULT_OPTIONS.supply;
    this._optionDisable = options.disable || DEFAULT_OPTIONS.disable;
    this._optionError = (options.error || DEFAULT_OPTIONS.error).bind(this);
    this._optionKey = options.key || void 0;
    // initialize misc attributes
    this.consumers = 0;
    // initialized within Aspect#_bind
    this.id = null;
    this.$store = null;
    this.$module = null;
    this._stateKeys = null;
    this._commitKeys = null;
    this._stateReady = false;
    // misc private attributes
    this._queryDataCtrl = new QueryData();
    this._cache = null;
    this._watcher = new BoundWatcher(this, onVariablesUpdate.bind(null, this), options.variables, options.watch);
    this.__onResponse = onResponse.bind(null, this);
    // expose helper utilities
    const {life, route} = mixins(this);
    this.mixin = this.lifeMixin = life;
    this.guards = this.routeMixin = route;
  }

  /**
   * @public
   * Removes one consumer count and disables the aspect if it was the last one.
   */
  release() {
    this.consumers--;
    if (this.consumers > 0) { return; }
    if (this.consumers < 0) {
      throw new Error("Aspect '" + this.id + "' has been released more often than grasped.");
    }
    this._watcher.stop();
    this._cache = null;
    this._optionDisable();
  }

  /**
   * @public
   * Adds one consumer count and enables the aspect if it was the first one. Ensures that the store attributes are not
   * empty.
   */
  async grasp() {
    if (!this.consumers++) { this._optionEnable(); }
    if (!this._stateReady) {
      await this.fetch();
      return;
    }
    if (!this._watcher.enabled) { this._performQuery(this._watcher.enable()); }
  }

  /**
   * @public
   * Triggers a new fetch.
   *
   * @param {Boolean} clearCache Whether to first remove the cache.
   * @returns {Promise<Object>} Resolves with the query data object.
   */
  async fetch(clearCache) {
    if (clearCache) { this.clearCache(); }
    if (this._cache != null) { return await this._cache; }
    // cache is synchronously ensured to be set
    const watcher = this._watcher;
    const promise = this._performQuery(watcher.enabled ? watcher.variables : watcher.enable());
    if (!this.consumers) { watcher.stop(); }
    return await promise;
  }

  /**
   * @public
   * Calls {@link Aspect#clearCache} and frees all references from the store.
   *
   * @returns {Object} The query data object.
   */
  clear() {
    this.clearCache();
    return onResponse(this, QueryData.clearTriggered(this._queryDataCtrl.next()));
  }

  /**
   * @public
   * Clears the cache. This enforces a re-fetch on next grasp.
   */
  clearCache() {
    this._stateReady = false;
    this._cache = null;
  }

  /**
   * @public
   * Injects an error or value to the store. Using promises enables race-condition drops similar to regular queries. If
   * `error` is or resolves with a `null`-alike value, `value` is looked-at instead. If either `error` or `value` (in
   * this priority) gets rejected, the rejection reason will be used as error result.
   *
   * @param {*|Promise<*>} value The value to inject (if error is or resolves to null).
   * @param {*|Promise<*>} error The error to inject.
   * @returns {Promise<Object>} Resolves with the query data object.
   */
  async inject(value, error) {
    const queryData = QueryData.injectionTriggered(this._queryDataCtrl.next());
    commitLoading(this, queryData);
    let promise;
    if (error == null) {
      promise = Promise.resolve(value);
    } else {
      promise = Promise.resolve(error).then((err) => err == null ? value : Promise.reject(err));
    }
    return this._cache = promise
      .then(QueryData.onSuccess.bind(null, queryData), QueryData.onError.bind(null, queryData))
      .then(this.__onResponse, this.__onResponse);
  }

  /**
   * @public
   * Creates a context object similar to vuex action context parameter.
   *
   * @return {{dispatch, commit, getters, state, rootGetters, rootState}} The created context object.
   */
  context() {
    const local = this.$module, store = this.$store;
    return {
      dispatch: local.dispatch, commit: local.commit,
      getters: local.getters, state: local.state, rootGetters: store.getters, rootState: store.state,
    };
  }

  /**
   * @private
   *
   * @param {*} variables The query variables
   * @returns {Promise<Object>} Resolves with the query data object when the query is finished.
   */
  _performQuery(variables) {
    const queryData = QueryData.withVariables(this._queryDataCtrl.next(), variables);
    if (typeof this._optionResolve !== "function") { return Promise.resolve(queryData); }
    commitLoading(this, queryData);
    return this._cache = Promise
      .resolve(this._optionResolve(variables))
      .then(QueryData.onSuccess.bind(null, queryData), QueryData.onError.bind(null, queryData))
      .then(this.__onResponse, this.__onResponse);
  }

  /**
   * @private
   *
   * @param {String} ns The module namespace to bind to.
   * @param {String} key The key to use.
   * @param {{state, getters, _vm, $watch: Function, _withCommit: Function, _subscribers: [Function]}} store The vuex
   * store instance.
   * @param {{state,context}} module The vuex store module.
   */
  _bind(ns, key, store, module) {
    key = this._optionKey || key;

    this.id = ns + key;

    this.$store = store;
    this.$module = module.context;

    this._stateKeys = getStateKeys(key);
    this._commitKeys = getCommitKeys(this.id);

    const state = module.state, keys = this._stateKeys;
    this._stateReady = state[keys.success] !== void 0 || state[keys.error] !== void 0 || state[keys.loading];

    commitInit(this);
  }

}

function onVariablesUpdate(aspect, data) {
  aspect._optionSupply(data);
  if (!data.isInitial) { aspect._performQuery(data.variables); }
}

function onResponse(aspect, queryData) {
  if (aspect._queryDataCtrl.checkDrop(queryData)) {
    if (queryData.hasFailed) { aspect._optionError(queryData); }
    return queryData;
  }
  commitResponse(aspect, queryData);
  if (aspect._cache != null) { aspect._stateReady = true; }
  if (queryData.hasFailed) { aspect._optionError(queryData); }
  return queryData;
}
