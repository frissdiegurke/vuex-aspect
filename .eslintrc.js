// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: "babel-eslint",
  parserOptions: {
    sourceType: "module"
  },
  env: {
    browser: true,
    es6: true
  },
  extends: ["eslint:recommended"],
  // add your custom rules here
  rules: {
    // allow debugger during development
    "no-cond-assign": ["error", "except-parens"],
    quotes: ["error", "double"],
    semi: ["error", "always"],
    curly: ["error", "all"],
    eqeqeq: ["error", "always", {null: "ignore"}],
    "no-alert": ["error"],
    "no-eval": ["error"],
    "no-extra-label": ["error"],
    "no-global-assign": ["error"],
    "no-implied-eval": ["error"],
    "no-invalid-this": ["error"],
    "no-lone-blocks": ["error"],
    "no-loop-func": ["error"],
    "no-multi-str": ["error"],
    "no-new-wrappers": ["error"],
    "no-octal-escape": ["error"],
    "no-throw-literal": ["error"],
    "no-useless-escape": ["error"],
    "wrap-iife": ["error"],
    strict: ["error", "global"],
    "no-path-concat": ["error"],
    "no-sync": ["error"],
    "new-cap": ["error"],
    "new-parens": ["error"],
    "quote-props": ["error", "as-needed"],
    "no-useless-computed-key": ["error"],
    "no-useless-constructor": ["error"],
    "no-var": ["error"],
    "object-shorthand": ["error"],
    "prefer-arrow-callback": ["error", {allowNamedFunctions: true}],
    "prefer-rest-params": ["error"],
    "no-extend-native": ["warn"],
    "no-extra-bind": ["warn"],
    "valid-jsdoc": ["warn", {requireReturn: false}],
    complexity: ["warn", {max: 12}],
    "max-depth": ["warn", {max: 4}],
    "max-len": ["warn", {code: 120}],
    "max-lines": ["warn", {max: 200, skipComments: true, skipBlankLines: true}],
    "no-multi-spaces": ["warn"],
    "no-useless-call": ["warn"],
    "handle-callback-err": ["warn", "^(err|error)$"],
    "brace-style": ["warn", "1tbs", {allowSingleLine: true}],
    camelcase: ["warn"],
    "eol-last": ["warn"],
    "no-multiple-empty-lines": ["warn", {max: 2}],
    "no-tabs": ["warn"],
    "prefer-reflect": ["warn", {exceptions: ["call"]}]
  }
};
