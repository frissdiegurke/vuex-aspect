// aspect definition
const myUserAspect = new VuexAspect.Aspect({
  variables({state}) { return {id: state.userId}; },
  supply({variables, isInitial}) {
    isInitial || unsubscribe();
    subscribe(variables, (err, data) => { this.inject(data, err); });
  },
  disable: unsubscribe,
});

// consumer component
const consumer = Vue.component("consumer", {
  mixins: [myUserAspect.mixin],
  template: "<pre><code>nom nom nom...</code></pre>",
});

// main component
new Vue({
  el: "#app",
  components: {consumer},
  methods: Vuex.mapMutations(["toggleConsumption", "incrementUser"]),
  computed: Vuex.mapState(["consume", "userId", "user", "user$error"]),
  store: new Vuex.Store({
    plugins: [VuexAspect.install],
    state: {userId: 0, consume: false},
    aspects: {user: myUserAspect},
    mutations: {
      toggleConsumption(state) { state.consume = !state.consume; },
      incrementUser(state) { state.userId = state.userId + 1; },
    },
  }),
});

// simulated server (while subscribed push one result every 100ms to 2s)

let timeout = null;
let count = 0;

function subscribe(variables, next) {
  timeout = setTimeout(response, 100 + Math.random() * 1900);

  function response() {
    if ((variables.id + 2) % 4 === 0) {
      next({count, status: 404, message: "User #" + variables.id + " does not exist."});
    } else {
      next(null, {count, id: "user-" + variables.id, name: "Jon Doe " + variables.id});
    }
    count++;
    timeout = setTimeout(response, 100 + Math.random() * 1900);
  }
}

function unsubscribe() { clearTimeout(timeout); }
