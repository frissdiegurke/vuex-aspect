// aspect definition
const myUserAspect = new VuexAspect.Aspect({
  variables({state}) { return {id: state.userId}; },
  resolve(variables) { return fetchUser(variables); },
});

// consumer component
const consumer = Vue.component("consumer", {
  mixins: [myUserAspect.mixin],
  template: "<pre><code>nom nom nom...</code></pre>",
});

// main component
new Vue({
  el: "#app",
  components: {consumer},
  methods: Vuex.mapMutations(["toggleConsumption", "incrementUser"]),
  computed: Vuex.mapState(["consume", "userId", "user", "user$loading", "user$error"]),
  store: new Vuex.Store({
    plugins: [VuexAspect.install],
    state: {userId: 0, consume: false},
    aspects: {user: myUserAspect},
    mutations: {
      toggleConsumption(state) { state.consume = !state.consume; },
      incrementUser(state) { state.userId = state.userId + 1; },
    },
  }),
});

// simulated server fetch
function fetchUser(data) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if ((data.id + 2) % 4 === 0) { return reject({status: 404, message: "User #" + data.id + " does not exist."}); }
      resolve({id: "user-" + data.id, name: "Jon Doe " + data.id});
    }, Math.random() * 6000);
  });
}
