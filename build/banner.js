/* eslint-disable no-console */
const pkg = require("../package.json");

console.log(`\
/*! ${pkg.name} v${pkg.version}
 * Copyright (c) 2018 Ole Reglitzki
 * Released under the MIT License.
 * ${pkg.repository.url}
 */`);
