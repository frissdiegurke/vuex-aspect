#!/usr/bin/env bash

BASE_DIR="$(cd "`dirname "$0"`" && pwd)"

BANNER_FILE="${BASE_DIR}/.banner.txt"

env node "${BASE_DIR}/banner.js" > "${BANNER_FILE}"

for file in "${BASE_DIR}"/../dist/*.js; do
  content="$(cat "${BASE_DIR}/.banner.txt" "${file}")"
  echo "${content}" > "${file}";
done

rm "${BANNER_FILE}"
